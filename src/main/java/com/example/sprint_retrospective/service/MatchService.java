package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.ServiceException;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.logic.Board;
import com.example.sprint_retrospective.logic.MoveResult;
import com.example.sprint_retrospective.logic.WinResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import java.util.List;

import static com.example.sprint_retrospective.ServiceException.*;


@Service
@Transactional
public class MatchService {
    final EntityManager entityManager;
    @Autowired
    BoardService boardService;
    @Autowired
    UserRepository userRepository;

    public MatchService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Match getMatch(Integer matchId) {
        return entityManager.find(Match.class, matchId);
    }

    public Match save(Match match) {
        entityManager.persist(match);
        return match;
    }

    public List<Match> listMatch(
            List<Match.MatchStatus> statuses,
            Integer userId,
            String orderBy, String orderIn) {
        String qlString = "SELECT b FROM Match b WHERE 1 = 1 AND ";
        if (statuses != null && !statuses.isEmpty()) {
            qlString += "b.status IN :statuses";
        }
        if (userId != null) {
            qlString += "(b.user1.id = :userId OR b.user2.id = :userId)";
        }
        if (orderBy != null) {
            qlString += " ORDER BY b." + orderBy;
        } else {
            qlString += " ORDER BY b.id";
        }
        if (orderIn != null) {
            qlString += " " + orderIn;
        } else {
            qlString += " DESC";
        }

        TypedQuery<Match> query = entityManager.createQuery(qlString, Match.class);
        if (statuses != null && !statuses.isEmpty()) {
            query.setParameter("statuses", statuses);
        }
        if (userId != null) {
            query.setParameter("userId", userId);
        }
        if (orderBy != null) {
            query.setParameter("orderBy", orderBy);
        }

        if (orderIn != null) {
            query.setParameter("orderIn", orderIn);
        }
        return query.getResultList();
    }

    public Match createMatch(Integer userId) {
        User user = entityManager.find(User.class, userId);
        var isPlaying = entityManager.createQuery("SELECT m FROM Match m WHERE m.status <> :status" +
                " AND (m.player1.id <> :userId AND m.player2.id <> :userId)")
                .setParameter("status", Match.MatchStatus.D_ENDED)
                .setParameter("userId", userId)
                .getResultList().size() > 0;
        if (isPlaying) {
            throw ServiceException.MATCH_PLAYING;
        }
        Match match = new Match();
        match.setStatus(Match.MatchStatus.A_WAITING);
        match.setPlayer1(user);
        entityManager.persist(match);
        entityManager.flush();
        return match;
    }


    public Boolean removeMatch(Integer matchId, Integer userId) {
        User user = entityManager.find(User.class, userId);
        Match match = entityManager.find(Match.class, matchId);
        if (match == null) {
            throw MATCH_NOT_FOUND;
        }
        if (!match.getStatus().equals(Match.MatchStatus.A_WAITING) && !match.getStatus().equals(
                Match.MatchStatus.B_HOLDING)) {
            throw MATCH_INVALID_STATUS;
        }
        if (!match.getPlayer1().getId().equals(userId)) {
            throw MATCH_INVALID_ACCESS;
        }
        entityManager.remove(match);
        entityManager.flush();
        return true;
    }

    public Match joinMatch(Integer matchId, Integer userId) {
        Match match = entityManager.find(Match.class, matchId);
        User user = entityManager.find(User.class, userId);

        if (!match.getStatus().equals(Match.MatchStatus.A_WAITING)) {
            throw MATCH_INVALID_STATUS;
        }
        if (match.getPlayerIds().contains(userId)) {
            throw MATCH_INVALID_ACCESS;
        }
        match.setPlayer2(user);
        match.setStatus(Match.MatchStatus.B_HOLDING);
        entityManager.persist(match);
        return match;
    }

    public Match startMatch(Integer MatchId, Integer userId) {
        Match match = entityManager.find(Match.class, MatchId);
        if (!match.getStatus().equals(Match.MatchStatus.B_HOLDING)) {
            throw MATCH_INVALID_STATUS;
        }

        if (!match.getPlayer1().getId().equals(userId)) {
            throw MATCH_INVALID_ACCESS;
        }
        match.setStatus(Match.MatchStatus.C_PLAYING);
        entityManager.persist(match);
        return match;
    }

    public MoveResult move(Integer matchId, Integer userId, Integer i, Integer j) {
        User user = entityManager.find(User.class, userId);
        Board board = boardService.getBoard(matchId);
        Match match = getMatch(matchId);
        if (!match.getStatus().equals(Match.MatchStatus.C_PLAYING)) {
            throw MATCH_INVALID_STATUS;
        }

        Byte side = getSide(match, user.getId());
        MoveResult result = board.move(side, i, j);
        WinResult winResult = result.getWinResult();
        Integer matchResult = null;
        if (winResult != null) {
            if (winResult.getSide() == 0) {
                System.out.println("MATCH DRAW");
                matchResult = 0;
            } else if (winResult.getSide() == 1) {
                System.out.println("PLAYER 1 WON");
                matchResult = 1;
            } else if (winResult.getSide() == 2) {
                System.out.println("PLAYER 2 WON");
                matchResult = 2;
            }
            endMatch(matchId, matchResult);
        }
        result.setMatch(match);
        return result;
    }

    public Match endMatch(Integer matchId, Integer matchResult) {
        Match match = getMatch(matchId);
        User player1 = match.getPlayer1();
        Integer player1Score = player1.getScore();
        User player2 = match.getPlayer2();
        Integer player2Score = player2.getScore();
        if (matchResult.equals(1)) {
            // PLAYER 1 WIN
            if (player1Score > player2Score) {
                player1.changeScore(5);
                player2.changeScore(-5);
                match.setPlayer1ScoreChange(5);
                match.setPlayer2ScoreChange(-5);
            } else if (player1Score < player2Score) {
                player1.changeScore(10);
                player2.changeScore(-10);
                match.setPlayer1ScoreChange(10);
                match.setPlayer2ScoreChange(-10);
            } else {
                player1.changeScore(5);
                player2.changeScore(-5);
                match.setPlayer1ScoreChange(5);
                match.setPlayer2ScoreChange(-5);
            }
        }
        else if (matchResult.equals(2)) {
            // PLAYER 2 WIN
            if (player1Score > player2Score) {
                player1.changeScore(-10);
                player2.changeScore(10);
                match.setPlayer1ScoreChange(-10);
                match.setPlayer2ScoreChange(10);
            } else if (player1Score < player2Score) {
                player1.changeScore(-5);
                player2.changeScore(5);
                match.setPlayer1ScoreChange(-5);
                match.setPlayer2ScoreChange(5);
            } else {
                player1.changeScore(-5);
                player2.changeScore(+5);
                match.setPlayer1ScoreChange(-5);
                match.setPlayer2ScoreChange(+5);
            }
        } else {
            // DRAW
            player1.changeScore(+5);
            player2.changeScore(+5);

        }
        match.setMatchResult(matchResult);
        match.setStatus(Match.MatchStatus.D_ENDED);
        match.setData(boardService.getBoard(matchId).serialize());
        save(match);
        userRepository.save(player1);
        userRepository.save(player2);
        entityManager.flush();
        return match;
    }

    public Match currentPlayingMatch(Integer userId) {
        List<Match> match = entityManager.createQuery("SELECT b FROM Match b where b.status <> :status" +
                " and (b.player1.id = :userId OR b.player2.id = :userId)", Match.class)
                .setParameter("status", Match.MatchStatus.D_ENDED)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();
        // Create new Match
        if (match.isEmpty()) {
            return null;
        }
        return match.get(0);
    }

    public Match quickJoin(Integer userId) {
        List<Match> match = entityManager.createQuery("SELECT b FROM Match b where b.status = :status" +
                " and b.player1.id <> :userId", Match.class)
                .setParameter("status", Match.MatchStatus.A_WAITING)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();
        // Create new Match
        if (match.isEmpty()) {
            return createMatch(userId);
        }
        return joinMatch(match.get(0).getId(), userId);
    }

    public Match resign(Integer matchId, Integer userId) {
        Match match = getMatch(matchId);
        User player1 = match.getPlayer1();
        Integer player1Score = player1.getScore();
        User player2 = match.getPlayer2();
        match.setMatchResult(2);
        match.setStatus(Match.MatchStatus.D_ENDED);
        if (!match.getPlayerIds().contains(userId)) {
            throw MATCH_INVALID_ACCESS;
        } else {
            String data = boardService.getBoard(matchId).serialize();
            if (userId.equals(player1.getId())) {
                match.setMatchResult(2);
                player1.changeScore(-10);
                player2.changeScore(10);
                match.setPlayer1ScoreChange(-10);
                match.setPlayer2ScoreChange(10);
            } else {
                match.setMatchResult(1);
                player1.changeScore(+10);
                player1.changeScore(-10);
                match.setPlayer1ScoreChange(10);
                match.setPlayer2ScoreChange(-10);
            }
            match.setStatus(Match.MatchStatus.D_ENDED);
            match.setData(data);
            save(match);
        }
        return match;
    }

    private Byte getSide(Match match, Integer userId) {
        if (!match.getPlayerIds().contains(userId)) {
            throw MATCH_INVALID_ACCESS;
        }
        if (userId.equals(match.getPlayer1().getId())) {
            return 1;
        } else {
            return 2;
        }
    }
}
