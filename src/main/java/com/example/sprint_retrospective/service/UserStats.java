package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.dto.SimpleUser;
import com.example.sprint_retrospective.dto.User;

public class UserStats {
    User user;
    Long totalMatch;
    Long winMatchAsFirstPlayer;
    Long winMatchAsSecondPlayer;
    Long drawMatch;

    public UserStats(User user, Long totalMatch, Long winMatchAsFirstPlayer, Long winMatchAsSecondPlayer, Long drawMatch) {
        this.user = user;
        this.totalMatch = totalMatch;
        this.winMatchAsFirstPlayer = winMatchAsFirstPlayer;
        this.winMatchAsSecondPlayer = winMatchAsSecondPlayer;
        this.drawMatch = drawMatch;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getTotalMatch() {
        return totalMatch;
    }

    public void setTotalMatch(Long totalMatch) {
        this.totalMatch = totalMatch;
    }

    public Long getWinMatchAsFirstPlayer() {
        return winMatchAsFirstPlayer;
    }

    public void setWinMatchAsFirstPlayer(Long winMatchAsFirstPlayer) {
        this.winMatchAsFirstPlayer = winMatchAsFirstPlayer;
    }

    public Long getWinMatchAsSecondPlayer() {
        return winMatchAsSecondPlayer;
    }

    public void setWinMatchAsSecondPlayer(Long winMatchAsSecondPlayer) {
        this.winMatchAsSecondPlayer = winMatchAsSecondPlayer;
    }

    public Long getDrawMatch() {
        return drawMatch;
    }

    public void setDrawMatch(Long drawMatch) {
        this.drawMatch = drawMatch;
    }
}
