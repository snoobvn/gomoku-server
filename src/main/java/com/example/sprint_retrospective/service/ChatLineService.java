package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.ServiceException;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.dto.ChatLine;
import com.example.sprint_retrospective.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
@Transactional
public class ChatLineService {
    @Autowired
    EntityManager entityManager;

    public List<ChatLine> listChatLine(Integer matchId, Integer limit, Integer offset) {
        String qlQuery = "SELECT cl FROM ChatLine cl";
        if (matchId != null) {
            qlQuery += " WHERE cl.match.id = :matchId";
        }
        qlQuery += " ORDER BY createdAt ASC";
        TypedQuery<ChatLine> query = entityManager.createQuery(qlQuery, ChatLine.class);
        if (matchId != null) {
            query.setParameter("matchId", matchId);
        }
        if (limit != null) {
            query.setMaxResults(limit);
        } else {
            query.setMaxResults(100);
        }

        if (offset != null) {
            query.setFirstResult(offset);
        } else {
            query.setFirstResult(0);
        }
        return query.getResultList();
    }

    public ChatLine createChatLine(Integer matchId, Integer userId, String content) {
        Match match = entityManager.find(Match.class, matchId);
        User user = entityManager.find(User.class, userId);

        if (!match.getPlayerIds().contains(userId)) {
            throw ServiceException.MATCH_INVALID_ACCESS;
        }
        ChatLine chatLine = new ChatLine(match, content, user);
        entityManager.persist(chatLine);
        return chatLine;
    }
}
