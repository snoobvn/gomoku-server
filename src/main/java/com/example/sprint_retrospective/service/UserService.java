package com.example.sprint_retrospective.service;

import com.example.sprint_retrospective.ServiceException;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.dto.VerificationToken;
import com.example.sprint_retrospective.event.OnRegistrationCompleteEvent;
import com.example.sprint_retrospective.util.SpringUtils;
import com.nimbusds.oauth2.sdk.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Transactional
public class UserService {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    SessionRegistry sessionRegistry;
    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    public User findById(Integer integer) {
        return userRepository.findById(integer).get();
    }

    public User create(String displayName, String email, String rawPassword) {
        return create(displayName, email, rawPassword, false);
    }

    public User updateInfo(Integer userId, String newPassword, String displayName) {
        Optional<User> oUser = userRepository.findById(userId);
        var user = oUser.get();
        if (!StringUtils.isBlank(newPassword)) {
            user.setPasswordChanged(true);
            user.setHashedPassword(passwordEncoder.encode(newPassword));
        }
        if (!StringUtils.isBlank(displayName)) {
            user.setDisplayName(displayName);
        }
        return userRepository.save(user);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public User create(String displayName, String email, String rawPassword, Boolean isAutoRegister) {
        String hashedPassword = passwordEncoder.encode(rawPassword);
        User user = getByEmail(email);
        if (user != null) {
            throw ServiceException.USER_EXIST;
        }
        User createdUser = new User(displayName, email, hashedPassword);
        if (isAutoRegister) {
            createdUser.setPasswordChanged(false);
            createdUser.setEnabled(true);
        }
        userRepository.saveAndFlush(createdUser);
        Optional<HttpServletRequest> request = SpringUtils.getCurrentHttpRequest();

        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(createdUser,
                request.map(ServletRequest::getLocale).orElse(Locale.ENGLISH),
                request.map(HttpServletRequest::getContextPath).orElse(""),
                isAutoRegister ? rawPassword : null
        ));
        return createdUser;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    private boolean emailExist(String email) {
        return userRepository.findByEmail(email) != null;
    }

    public VerificationToken getVerificationToken(String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    public User enableUser(User user) {
        user.setEnabled(true);
        return userRepository.save(user);
    }

    public User lockUser(Integer userId) {
        var user = userRepository.findById(userId).get();
        List principals = sessionRegistry.getAllPrincipals();
        for (Object principal : principals) {
            var email = "";
            if (principal instanceof DefaultOAuth2User) {
                email = ((DefaultOAuth2User) principal).getAttribute("email");
            }
            if (principal instanceof org.springframework.security.core.userdetails.User) {
                email = ((org.springframework.security.core.userdetails.User) principal).getUsername();
            }
            if (email.equals(user.getEmail())) {
                List<SessionInformation> sessionInformations = sessionRegistry.getAllSessions(principal, false);
                for (SessionInformation information : sessionInformations) {
                    information.expireNow();
                }
            }
        }
        user.setLocked(true);
        return userRepository.save(user);
    }

    public UserStats getUserStats(Integer userId) {
        var user = userRepository.findById(userId).get();
        Long totalMatch = (Long) entityManager.createQuery("select count(m) from Match m " +
                "where (m.player1.id = :userId  or m.player2.id = :userId) and m.status = :status")
                .setParameter("userId", userId)
                .setParameter("status", Match.MatchStatus.D_ENDED)
                .getSingleResult();
        Long winMatchAsFirstPlayer = (Long) entityManager.createQuery("select count(m) from Match m " +
                "where (m.player1.id = :userId) and m.status = :status and m.matchResult = 1")
                .setParameter("userId", userId)
                .setParameter("status", Match.MatchStatus.D_ENDED)
                .getSingleResult();

        Long winMatchAsSecondPlayer = (Long) entityManager.createQuery("select count(m) from Match m " +
                "where (m.player2.id = :userId) and m.status = :status and m.matchResult = 2")
                .setParameter("userId", userId)
                .setParameter("status", Match.MatchStatus.D_ENDED)
                .getSingleResult();
        Long drawMatch = (Long) entityManager.createQuery("select count(m) from Match m " +
                "where (m.player1.id = :userId  or m.player2.id = :userId) and m.status = :status and m.matchResult = 0")
                .setParameter("userId", userId)
                .setParameter("status", Match.MatchStatus.D_ENDED)
                .getSingleResult();
        return new UserStats(user, totalMatch, winMatchAsFirstPlayer, winMatchAsSecondPlayer, drawMatch);
    }
}
