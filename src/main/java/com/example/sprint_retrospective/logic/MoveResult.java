package com.example.sprint_retrospective.logic;

import com.example.sprint_retrospective.dto.Match;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class MoveResult {
    private boolean success;
    private WinResult winResult;
    @JsonIgnore
    private Match match;

    public MoveResult(boolean success, WinResult winResult) {
        this.success = success;
        this.winResult = winResult;
    }

    public MoveResult(boolean success, WinResult winResult, Match match) {
        this.success = success;
        this.winResult = winResult;
        this.match = match;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public WinResult getWinResult() {
        return winResult;
    }

    public void setWinResult(WinResult winResult) {
        this.winResult = winResult;
    }

}
