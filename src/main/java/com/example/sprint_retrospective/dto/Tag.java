package com.example.sprint_retrospective.dto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table
public class Tag {
    public static enum TagType {
        WENT_WELL, TO_IMPROVE, ACTION_ITEM
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(max = 1000)
    private String content;

    @ManyToOne
    private Match match;

    @Enumerated(EnumType.STRING)
    @NotNull
    private TagType tagType;

    public Tag() {
    }

    public Tag(String content, Match match, @NotNull TagType tagType) {
        this.content = content;
        this.match = match;
        this.tagType = tagType;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Match getBoard() {
        return match;
    }

    public void setBoard(Match match) {
        this.match = match;
    }

    public TagType getTagType() {
        return tagType;
    }

    public void setTagType(TagType tagType) {
        this.tagType = tagType;
    }
}
