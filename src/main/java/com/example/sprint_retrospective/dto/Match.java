package com.example.sprint_retrospective.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name = "`match`")
public class Match {
    public enum MatchStatus {
        A_WAITING, B_HOLDING, C_PLAYING, D_ENDED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @NotNull
    private MatchStatus status = MatchStatus.A_WAITING;

    @ManyToOne
    private User player1;

    private Integer player1ScoreChange;

    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true,mappedBy = "match")
    @JsonIgnore
    private Set<ChatLine> chatLines;

    @ManyToOne
    private User player2;

    private Integer player2ScoreChange;

    private Integer matchResult;

    @CreationTimestamp
    private Date createdAt;

    @Size(max = 1000)
    private String data;

    public Match() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MatchStatus getStatus() {
        return status;
    }

    public void setStatus(MatchStatus status) {
        this.status = status;
    }

    public User getPlayer1() {
        return player1;
    }

    public void setPlayer1(User user1) {
        this.player1 = user1;
    }

    public User getPlayer2() {
        return player2;
    }

    public void setPlayer2(User user2) {
        this.player2 = user2;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Integer getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(Integer matchResult) {
        this.matchResult = matchResult;
    }

    public Set<Integer> getPlayerIds(){
        Set<Integer> playerIds = new HashSet<>();
        if(player1 != null){
            playerIds.add(player1.getId());
        }
        if(player2 != null){
            playerIds.add(player2.getId());
        }
        return playerIds;
    }

    public Integer getPlayer1ScoreChange() {
        return player1ScoreChange;
    }

    public void setPlayer1ScoreChange(Integer player1ScoreChange) {
        this.player1ScoreChange = player1ScoreChange;
    }

    public Integer getPlayer2ScoreChange() {
        return player2ScoreChange;
    }

    public void setPlayer2ScoreChange(Integer player2ScoreChange) {
        this.player2ScoreChange = player2ScoreChange;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
