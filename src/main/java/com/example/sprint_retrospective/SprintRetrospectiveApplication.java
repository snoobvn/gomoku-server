package com.example.sprint_retrospective;

import com.example.sprint_retrospective.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.persistence.EntityManager;

@SpringBootApplication
@EnableAsync
public class SprintRetrospectiveApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SprintRetrospectiveApplication.class, args);
    }

    @Autowired
    EntityManager entityManager;
    @Autowired
    UserService userService;

    @Override
    public void run(String... args) throws Exception {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
