package com.example.sprint_retrospective.event;

import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RegistrationListener implements
        ApplicationListener<OnRegistrationCompleteEvent> {

    @Autowired
    private UserService service;

    @Autowired
    private MailSender mailSender;

    @Override
    @Async
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    @Async
    public void confirmRegistration(OnRegistrationCompleteEvent event) {
        try {
            System.out.println("Sending Email ...");
            User user = event.getUser();
            String token = UUID.randomUUID().toString();
            service.createVerificationToken(user, token);

            String recipientAddress = user.getEmail();
            String subject = "Registration Confirmation";
            String confirmationUrl
                    = event.getAppUrl() + "/user/emailconfirm?token=" + token;
            String text = "Email Confirmation";
            if (event.getDefaultPassword() != null) {
                text += "\n\nThis account is automatically registered, your defaultPassword is " + event.getDefaultPassword();
            } else {
                text += "\n\n Activation Link: " + confirmationUrl;
            }
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setTo(recipientAddress);
            simpleMailMessage.setSubject(subject);
            simpleMailMessage.setText(text);
            mailSender.send(simpleMailMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}