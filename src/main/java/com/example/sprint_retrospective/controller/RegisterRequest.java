package com.example.sprint_retrospective.controller;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class RegisterRequest {
    @Email
    private String email;
    @NotNull
    private String displayName;
    @NotNull
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
