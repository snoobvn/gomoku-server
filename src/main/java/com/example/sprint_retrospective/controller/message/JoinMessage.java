package com.example.sprint_retrospective.controller.message;

import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.dto.SimpleUser;

public class JoinMessage extends MatchMessage {

    private SimpleUser user;

    public JoinMessage() {
        super();
    }

    public JoinMessage(Match match, SimpleUser user) {
        super(match, null);
        this.user = user;
    }

    public SimpleUser getUser() {
        return user;
    }

    public void setUser(SimpleUser user) {
        this.user = user;
    }
}