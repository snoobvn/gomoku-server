package com.example.sprint_retrospective.controller.message;

import com.example.sprint_retrospective.dto.Match;

public class ChatMessage extends MatchMessage {
    private String content;

    public ChatMessage() {
    }

    public ChatMessage(Match match, String content) {
        super(match, null);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
