package com.example.sprint_retrospective.controller.message;

import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.logic.Board;

public class ResignMessage extends MatchMessage{
    public ResignMessage(Match match) {
        super(match, null);
    }
}
