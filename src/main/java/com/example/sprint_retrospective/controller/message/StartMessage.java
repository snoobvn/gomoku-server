package com.example.sprint_retrospective.controller.message;

import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.logic.Board;

public class StartMessage extends MatchMessage {
    public StartMessage(Match match, Board board) {
        super(match, board);
    }
}
