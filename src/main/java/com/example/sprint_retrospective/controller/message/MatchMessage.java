package com.example.sprint_retrospective.controller.message;

import com.example.sprint_retrospective.Constant;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.logic.Board;

public class MatchMessage {
    private Match match;
    private Integer side;
    private String data;

    public MatchMessage() {
    }

    public MatchMessage(Match match, Board board) {
        this.match = match;
        if (board == null) {
            this.side = 0;
            this.data = new Board(Constant.boardSize, Constant.winCell).serialize();
        } else {
            this.side = board.lastSide;
            this.data = board.serialize();
        }
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Integer getSide() {
        return side;
    }

    public void setSide(Integer side) {
        this.side = side;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
