package com.example.sprint_retrospective.controller.admin;

import com.example.sprint_retrospective.dto.ChatLine;
import com.example.sprint_retrospective.dto.Match;
import com.example.sprint_retrospective.dto.SimpleUser;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.service.ChatLineService;
import com.example.sprint_retrospective.service.UserService;
import com.example.sprint_retrospective.service.UserStats;
import com.nimbusds.oauth2.sdk.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/admin")
@Transactional
public class AdminBoardController {
    @Autowired
    UserService userService;
    @Autowired
    ChatLineService chatLineService;
    @Autowired
    EntityManager entityManager;


    @PostMapping("lock")
    public User lockUser(@RequestParam Integer userId) {
        return userService.lockUser(userId);
    }

    @GetMapping("chat/{matchId}")
    public List<ChatLine> listChat(@PathVariable Integer matchId) {
        return chatLineService.listChatLine(matchId, 1000, 0);
    }

    @GetMapping("matches")
    public List<Match> listMatches(@RequestParam(required = false) Integer userId, @RequestParam(required = false) String search) {
        var sql = "select m from Match m where m.status = :status ";
        if (userId != null) {
            sql += "and (m.player1.id = :userId or m.player2.id = :userId ";
        }
        if (!StringUtils.isBlank(search)) {
            sql += "and (m.player1.displayName like :search or m.player1.email like :search " +
                    "or m.player2.displayName like :search or m.player2.displayName like :search)";
        }
        sql += " order by m.id desc";
        TypedQuery<Match> query = entityManager.createQuery(sql, Match.class).setParameter("status",
                Match.MatchStatus.D_ENDED);
        if (userId != null) {
            query = query.setParameter("userId", userId);
        }
        if (!StringUtils.isBlank(search)) {
            query = query.setParameter("search", "%" + search + "%");
        }
        return query
                .setMaxResults(100)
                .getResultList();
    }

    @GetMapping("users")
    public List<SimpleUser> listUsers(@RequestParam(required = false) String email, @RequestParam(required = false) String displayName) {
        var sql = "select u from User u where 1 = 1 ";
        if (!StringUtils.isBlank(email)) {
            sql += "and u.email like :email ";
        }
        if (!StringUtils.isBlank(displayName)) {
            sql += "and u.displayName like :displayName ";
        }
        sql += " order by u.id desc";
        TypedQuery<User> query = entityManager.createQuery(sql, User.class);
        if (!StringUtils.isBlank(email)) {
            query = query.setParameter("email", "%" + email + "%");
        }
        if (!StringUtils.isBlank(displayName)) {
            query = query.setParameter("displayName", "%" + displayName + "%");
        }
        return query
                .setMaxResults(100)
                .getResultList()
                .stream().map(u -> new SimpleUser(u))
                .collect(Collectors.toList())
                ;
    }

    @GetMapping("users/{userId}/stats")
    public UserStats stats(@PathVariable Integer userId) {
        return userService.getUserStats(userId);
    }

    @GetMapping("match/{id}")
    public Match getMatch(@PathVariable Integer id) {
        List<Match> matches = entityManager.createQuery("select m from Match m " +
                "where m.id = :id", Match.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (matches.isEmpty()) {
            return null;
        }
        return matches.get(0);
    }
}
