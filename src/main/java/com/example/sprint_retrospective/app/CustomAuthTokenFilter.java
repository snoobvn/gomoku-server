package com.example.sprint_retrospective.app;

import com.example.sprint_retrospective.ServiceException;
import com.example.sprint_retrospective.dto.User;
import com.example.sprint_retrospective.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.UUID;

@Component
public class CustomAuthTokenFilter extends GenericFilterBean {
    @Autowired
    UserService userService;
    @Autowired
    @Qualifier("myAuth")
    AuthenticationManager authenticationManager;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken = null;
        if (authentication instanceof OAuth2AuthenticationToken) {
            OAuth2AuthenticationToken oauth2Token = (OAuth2AuthenticationToken) authentication;
            String email = oauth2Token.getPrincipal().getAttribute("email");
            if (email != null) {
                User user = userService.getByEmail(email);
                if (user == null) {
                    user = userService.create(email.split("@")[0], email, UUID.randomUUID().toString(), true);
                }
//                checkLocked(response, user);
                ((OAuth2AuthenticationToken) authentication).setDetails(user);
                preAuthenticatedAuthenticationToken = new PreAuthenticatedAuthenticationToken(user,
                        "", authentication.getAuthorities());
                preAuthenticatedAuthenticationToken.setDetails(user);
            }
        } else if (authentication instanceof UsernamePasswordAuthenticationToken) {
            var user = ((org.springframework.security.core.userdetails.User) authentication.getPrincipal());
            User byEmail = userService.getByEmail(user.getUsername());
//            checkLocked(response, byEmail);
            preAuthenticatedAuthenticationToken = new PreAuthenticatedAuthenticationToken(byEmail,
                    "", authentication.getAuthorities());
            preAuthenticatedAuthenticationToken.setDetails(byEmail);
        }
        if (preAuthenticatedAuthenticationToken != null) {
            try {
                Authentication authenticate = authenticationManager.authenticate(preAuthenticatedAuthenticationToken);
                SecurityContext sc = SecurityContextHolder.getContext();
                sc.setAuthentication(authenticate);
            } catch (DisabledException ex) {
                throw ServiceException.USER_ACTIVATE;
            } catch (LockedException ex) {
                throw ServiceException.USER_BANNED;
            }
        }
        chain.doFilter(request, response);
    }

    private void checkLocked(HttpServletResponse response, User user) {

        if (user.getLocked()) {

        }
        if (!user.getEnabled()) {
            response.setStatus(403);
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(response.getOutputStream());
                outputStreamWriter.write("User Not Activated");
                outputStreamWriter.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }

    }
}