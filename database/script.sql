create table hibernate_sequence
(
    next_val bigint null
);

create table user
(
    id               int auto_increment
        primary key,
    display_name     varchar(255) null,
    email            varchar(255) null,
    hashed_password  varchar(255) null,
    locked           bit null,
    score            int null,
    enabled          bit null,
    password_changed bit null
);

create table `match`
(
    id                  int auto_increment
        primary key,
    created_at          datetime(6) null,
    data                varchar(1000) null,
    match_result        varchar(255) null,
    status              varchar(255) not null,
    player1_id          int null,
    player2_id          int null,
    player1score_change int null,
    player2score_change int null,
    constraint FKfbk1d8ksonclbvhfjigwjqon2
        foreign key (player2_id) references user (id),
    constraint FKhdo1052uxow42u1ds6piqyvom
        foreign key (player1_id) references user (id)
);

create table chat_line
(
    id            int auto_increment
        primary key,
    content       varchar(1000) null,
    created_at    datetime(6) null,
    created_by_id int null,
    match_id      int null,
    constraint FKbad4gdm9x5o92mcl0jd0gaxls
        foreign key (match_id) references `match` (id),
    constraint FKnv8hp22jnr40tp800kq4jb5uw
        foreign key (created_by_id) references user (id)
);

create table tag
(
    id       int auto_increment
        primary key,
    content  varchar(1000) null,
    tag_type varchar(255) not null,
    match_id int null,
    constraint FKo9it8lqrui729rl9o7bgehkyw
        foreign key (match_id) references `match` (id)
);

create table verification_token
(
    id          bigint not null
        primary key,
    expiry_date datetime(6) null,
    token       varchar(255) null,
    user_id     int null,
    constraint FKrdn0mss276m9jdobfhhn2qogw
        foreign key (user_id) references user (id)
);

